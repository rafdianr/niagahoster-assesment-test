import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import NiagaNavbar from "../layout/NiagaNavbar";
import Footer from "../layout/Footer";
import HomePage from "../pages/HomePage";

const Routes = () => {
  return (
    <Fragment>
      <NiagaNavbar />
      <Route path="/" component={HomePage} exact />

      <Footer />
    </Fragment>
  );
};

export default Routes;
