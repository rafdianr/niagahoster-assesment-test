import React from "react";
import "../assets/style/NiagaBantuan.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCommentAlt } from "@fortawesome/free-solid-svg-icons";

const NiagaBantuan = () => {
  return (
    <section className="nh-bantuan">
      <div className="container">
        <div className="bantuan-contents">
          <div className="bantuan-text">
            <h3>
              Perlu <strong>BANTUAN?</strong> Hubungi Kami:{" "}
              <strong>0274-5305505</strong>
            </h3>
          </div>
          <div className="bantuan-btn">
            {/* nanti diedit lagi untuk css btn nya pake css yg sama dengan btn yg lainnya */}
            <button className="btn-bantuan">
              <span>
                <FontAwesomeIcon icon={faCommentAlt} />
              </span>{" "}
              Live chat
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaBantuan;
