import React from "react";
import "../assets/style/NiagaPowerful.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";

const NiagaPowerful = () => {
  return (
    <section className="nh-powerful">
      <div className="container">
        <h3 className="heading-powerful">
          Powerful dengan Limit PHP yang Lebih Besar
        </h3>
        <div className="powerful-contents">
          <div className="powerful-item">
            <div className="powerful-icon">
              <span>
                <FontAwesomeIcon icon={faCheckCircle} />
              </span>
            </div>
            <div className="powerful-text">
              <h5>max execution time 300s</h5>
            </div>
          </div>
          <div className="powerful-item">
            <div className="powerful-icon">
              <span>
                <FontAwesomeIcon icon={faCheckCircle} />
              </span>
            </div>
            <div className="powerful-text">
              <h5>post max size 128 MB</h5>
            </div>
          </div>
        </div>
        <div className="powerful-contents ">
          <div className="powerful-item center">
            <div className="powerful-icon">
              <span>
                <FontAwesomeIcon icon={faCheckCircle} />
              </span>
            </div>
            <div className="powerful-text">
              <h5>max execution time 300s</h5>
            </div>
          </div>
          <div className="powerful-item center">
            <div className="powerful-icon">
              <span>
                <FontAwesomeIcon icon={faCheckCircle} />
              </span>
            </div>
            <div className="powerful-text">
              <h5>upload max file size 128 MB</h5>
            </div>
          </div>
        </div>
        <div className="powerful-contents">
          <div className="powerful-item">
            <div className="powerful-icon">
              <span>
                <FontAwesomeIcon icon={faCheckCircle} />
              </span>
            </div>
            <div className="powerful-text">
              <h5>php memory limit 1024 MB</h5>
            </div>
          </div>
          <div className="powerful-item">
            <div className="powerful-icon">
              <span>
                <FontAwesomeIcon icon={faCheckCircle} />
              </span>
            </div>
            <div className="powerful-text">
              <h5>max input vars 2500</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaPowerful;
