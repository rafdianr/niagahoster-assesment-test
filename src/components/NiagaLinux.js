import React from "react";
import "../assets/style/NiagaLinux.css";

const NiagaLinux = () => {
  return (
    <section className="linux">
      <div className="container-linux">
        <div className="content-linux">
          <div className="linux-left">
            <h2>Linux Hosting yang Stabil dengan Teknologi LVE</h2>
            <p>
              SuperMicro <strong>Intel Xeon 24-Cores</strong> server dengan RAM{" "}
              <strong>128 GB</strong> dan teknologi{" "}
              <strong>LVE CloudLinux</strong> untuk stabilitas server Anda.
              Dilengkapi dengan <strong>SSD</strong> untuk kecepatan{" "}
              <strong>MySQL</strong> dan caching, Apache load balancer berbasis
              LiteSpeed Technologies, <strong>CageFS</strong> security,{" "}
              <strong>Raid-10</strong> protection dan auto backup untuk keamanan
              website PHP Anda.
            </p>
            <button className="btn-linux">Pilih Hosting Anda</button>
          </div>
          <div className="linux-right">
            <div className="img-linux"></div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaLinux;
