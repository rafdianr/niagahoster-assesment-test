import React from "react";
import "../assets/style/NiagaModul.css";

const NiagaModul = () => {
  return (
    <section className="nh-modul">
      <div className="container">
        <h3 className="heading-modul">
          Modul lengkap untuk menjalankan aplikasi PHP Anda
        </h3>
        <div className="modul-contents">
          <div className="modul-item">
            <h5>IcePHP</h5>
            <h5>apc</h5>
            <h5>apcu</h5>
            <h5>apm</h5>
            <h5>ares</h5>
            <h5>bcmath</h5>
            <h5>bcompiler</h5>
            <h5>big_int</h5>
            <h5>bitset</h5>
            <h5>bloomy</h5>
            <h5>bz2_filter</h5>
            <h5>clamav</h5>
            <h5>coin_acceptor</h5>
            <h5>crack</h5>
            <h5>dba</h5>
          </div>
          <div className="modul-item">
            <h5>http</h5>
            <h5>huffman</h5>
            <h5>idn</h5>
            <h5>igbinary</h5>
            <h5>imagick</h5>
            <h5>imap</h5>
            <h5>inclued</h5>
            <h5>inotify</h5>
            <h5>interbase</h5>
            <h5>intl</h5>
            <h5>ioncube_loader</h5>
            <h5>ioncube_loader_4</h5>
            <h5>jsmin</h5>
            <h5>json</h5>
            <h5>ldap</h5>
          </div>
          <div className="modul-item">
            <h5>nd_pdo_mysql</h5>
            <h5>oauth</h5>
            <h5>oci8</h5>
            <h5>odbc</h5>
            <h5>opcache</h5>
            <h5>pdf</h5>
            <h5>pdo</h5>
            <h5>pdo_dblib</h5>
            <h5>pdo_firebird</h5>
            <h5>pdo_mysql</h5>
            <h5>pdo_odbc</h5>
            <h5>pdo_pgsql</h5>
            <h5>pdo_sqlite</h5>
            <h5>pgsql</h5>
            <h5>phalcon</h5>
          </div>
          <div className="modul-item">
            <h5>stats</h5>
            <h5>stem</h5>
            <h5>stomp</h5>
            <h5>suhosin</h5>
            <h5>sybase_ct</h5>
            <h5>sysvmsg</h5>
            <h5>sysvsem</h5>
            <h5>sysvshm</h5>
            <h5>tidy</h5>
            <h5>timezonedb</h5>
            <h5>trader</h5>
            <h5>translit</h5>
            <h5>uploadprogress</h5>
            <h5>uri_template</h5>
            <h5>uuid</h5>
          </div>
        </div>
        <div className="modul-btn">
          <button className="btn-modul">Selengkapnya</button>
        </div>
      </div>
    </section>
  );
};

export default NiagaModul;
