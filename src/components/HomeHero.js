import React from "react";
import "../assets/style/HomeHero.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";

const HomeHero = () => {
  return (
    <section className="hero">
      <div className="container-hero">
        <div className="hero-left">
          <h1>PHP Hosting</h1>
          <h2>Cepat, handal, penuh dengan modul PHP yang Anda butuhkan</h2>
          <div className="hero-list">
            <ul className="hero-list-item">
              <li className="hero-list-item-content">
                <span>
                  <FontAwesomeIcon icon={faCheckCircle} />
                </span>
                Solusi PHP untuk performa query yang lebih cepat.
              </li>
              <li className="hero-list-item-content">
                <span>
                  <FontAwesomeIcon icon={faCheckCircle} />
                </span>
                Konsumsi memori yang lebih rendah.
              </li>
              <li className="hero-list-item-content">
                <span>
                  <FontAwesomeIcon icon={faCheckCircle} />
                </span>
                Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7
              </li>
              <li className="hero-list-item-content">
                <span>
                  <FontAwesomeIcon icon={faCheckCircle} />
                </span>
                Fitur enkripsi IonCube dan Zend Guard Loaders
              </li>
            </ul>
          </div>
        </div>
        <div className="hero-right">
          <div className="img-rmd"></div>
        </div>
      </div>
    </section>
  );
};

export default HomeHero;
