import React from "react";
import "../assets/style/NiagaSemua.css";

const NiagaSemua = () => {
  return (
    <section className="nh-semua">
      <div className="container">
        <div className="divider"></div>
        <h3 className="heading-semua">Semua Paket Hosting Sudah Termasuk</h3>
        <div className="semua-contents">
          <div className="semua-item">
            <div className="semua-icon">
              <img
                src={require("../assets/images/icon_PHP_Hosting_PHP_Semua_Versi.svg")}
                alt=""
              />
            </div>
            <h4 className="semua-title">PHP Semua Versi</h4>
            <p className="semua-deskripsi">
              Pilih mulai dari versi PHP 5.3 s/d PHP 7. Ubah sesuka Anda!
            </p>
          </div>
          <div className="semua-item">
            <div className="semua-icon">
              <img
                src={require("../assets/images/icon_PHP_Hosting_MySQL.svg")}
                alt=""
              />
            </div>
            <h4 className="semua-title">MySQL Versi 5.6</h4>
            <p className="semua-deskripsi">
              Nikmati MySQL versi terbaru, tercepat dan kaya akan fitur.
            </p>
          </div>
          <div className="semua-item">
            <div className="semua-icon">
              <img
                src={require("../assets/images/icon_PHP_Hosting_CPanel.svg")}
                alt=""
              />
            </div>
            <h4 className="semua-title">Panel Hosting cPanel</h4>
            <p className="semua-deskripsi">
              Kelola website dengan panel canggih yang familiar di hati Anda.
            </p>
          </div>
        </div>
        <div className="semua-contents">
          <div className="semua-item">
            <div className="semua-icon">
              <img
                src={require("../assets/images/icon_PHP_Hosting_garansi_uptime.svg")}
                alt=""
              />
            </div>
            <h4 className="semua-title">Garansi Uptime 99.9%</h4>
            <p className="semua-deskripsi">
              Data center yang mendukung kelangsungan website Anda 24/7.
            </p>
          </div>
          <div className="semua-item">
            <div className="semua-icon">
              <img
                src={require("../assets/images/icon_PHP_Hosting_InnoDB.svg")}
                alt=""
              />
            </div>
            <h4 className="semua-title">Database InnoDB Unlimited</h4>
            <p className="semua-deskripsi">
              Jumlah dan ukuran database yang tumbuh sesuai kebutuhan Anda.
            </p>
          </div>
          <div className="semua-item">
            <div className="semua-icon">
              <img
                src={require("../assets/images/icon_PHP_Hosting_My_SQL_remote.svg")}
                alt=""
              />
            </div>
            <h4 className="semua-title">Wildcard Remote MySQL</h4>
            <p className="semua-deskripsi">
              Mendukung s/d 25 max_user_connections dan 100 max_connections.
            </p>
          </div>
        </div>
        <div className="divider"></div>
      </div>
    </section>
  );
};

export default NiagaSemua;
