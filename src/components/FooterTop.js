import React, { Fragment } from "react";

const FooterTop = (props) => {
  const list = props.list.map((item) => (
    <div className="top" key={item.title}>
      <h5 className="footer-title">{item.title}</h5>
      <ul className="footer-list">
        {item.item.map((x) => (
          <li key={x}>{x}</li>
        ))}
      </ul>
    </div>
  ));

  return <Fragment>{list}</Fragment>;
};

export default FooterTop;
