import React from "react";
import "../assets/style/NiagaMendukung.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";

const NiagaMendukung = () => {
  return (
    <section className="mendukung">
      <div className="container-mendukung">
        <h3 className="heading-mendukung">Mendukung Penuh Framework Laravel</h3>
        <div className="content-mendukung">
          <div className="mendukung-left">
            <h2>
              Tak perlu menggunakan dedicated server ataupun VPS yang mahal.
              Layanan PHP hosting murah kami mendukung penuh framework favorit
              Anda
            </h2>
            <div className="mendukung-list">
              <ul className="mendukung-list-item">
                <li className="mendukung-list-item-content">
                  <span>
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  Install Laravel <strong>1 klik</strong> dengan Softaculous
                  Installer.
                </li>
                <li className="mendukung-list-item-content">
                  <span>
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  Mendukung ekstensi{" "}
                  <strong>PHP MCrypt, phar, mbstring, json,</strong> dan{" "}
                  <strong>fileinfo.</strong>
                </li>
                <li className="mendukung-list-item-content">
                  <span>
                    <FontAwesomeIcon icon={faCheckCircle} />
                  </span>
                  Tersedia <strong>Composer</strong> dan <strong>SSH</strong>{" "}
                  untuk menginstal packages pilihan Anda.
                </li>
              </ul>
            </div>
            <p>
              Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis
            </p>
            <button className="btn-mendukung">Pilih Hosting Anda</button>
          </div>
          <div className="mendukung-right">
            <div className="img-mendukung"></div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaMendukung;
