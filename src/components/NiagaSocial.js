import React from "react";
import "../assets/style/NiagaSocial.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookSquare,
  faTwitterSquare,
  faGooglePlusSquare,
} from "@fortawesome/free-brands-svg-icons";

const NiagaSocial = () => {
  return (
    <section className="nh-social">
      <div className="container">
        <div className="social-contents">
          <div className="social-text">
            <h5>Bagikan jika Anda menyukai halaman ini.</h5>
          </div>
          <div className="social-icon">
            <span className="facebook">
              <FontAwesomeIcon icon={faFacebookSquare} />
            </span>
            <span className="twitter">
              <FontAwesomeIcon icon={faTwitterSquare} />
            </span>
            <span className="google">
              <FontAwesomeIcon icon={faGooglePlusSquare} />
            </span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaSocial;
