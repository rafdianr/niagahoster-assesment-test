import React, { Fragment } from "react";
import HomeHero from "../components/HomeHero";
import NiagaBantuan from "../components/NiagaBantuan";
import NiagaPackage from "../components/NiagaPackage";
import NiagaMendukung from "../components/NiagaMendukung";
import NiagaLinux from "../components/NiagaLinux";
import NiagaSocial from "../components/NiagaSocial";
import NiagaModul from "../components/NiagaModul";
import NiagaPowerful from "../components/NiagaPowerful";
import NiagaSemua from "../components/NiagaSemua";

const HomePage = () => {
  return (
    <Fragment>
      <HomeHero />
      <NiagaPackage />
      <NiagaPowerful />
      <NiagaSemua />
      <NiagaMendukung />
      <NiagaModul />
      <NiagaLinux />
      <NiagaSocial />
      <NiagaBantuan />
    </Fragment>
  );
};

export default HomePage;
