import React from "react";
import { Link } from "react-router-dom";
import "../assets/style/NiagaNavbar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faCommentAlt,
  faUserCircle,
} from "@fortawesome/free-solid-svg-icons";
import NhLogo from "../assets/images/nh-logo.svg";

const NiagaNavbar = () => {
  return (
    <section className="navbar">
      <div className="container">
        <ul className="navbar-top">
          <li className="topnav">
            <span>
              <FontAwesomeIcon icon={faPhone} />
            </span>
            0274-2885822
          </li>
          <li className="topnav">
            <span>
              <FontAwesomeIcon icon={faCommentAlt} />
            </span>
            Live Chat
          </li>
          <li className="topnav-user">
            <span>
              <FontAwesomeIcon icon={faUserCircle} />
            </span>
            Member Area
          </li>
        </ul>
        <div className="navbar-bottom">
          <Link className="logo-link" to="/">
            <img className="nh-logo" src={NhLogo} alt="logo" />
          </Link>
          <div className="navbar-bottom-list">
            <ul className="navbar-list">
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Hosting
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Domain
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Server
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Website
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Afiliasi
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Promo
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Pembayaran
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Review
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Kontak
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/" className="nv-list-item">
                  Blog
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaNavbar;
