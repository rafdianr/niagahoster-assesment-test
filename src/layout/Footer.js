import React, { useState } from "react";
import "../assets/style/FooterBottom.css";
import FooterTop from "../components/FooterTop";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

const Footer = () => {
  const [list1] = useState([
    {
      title: "Layanan",
      item: [
        "Domain",
        "Shared Hosting",
        "Cloud VPS Hosting",
        "Managed VPS Hosting",
        "Web Builder",
        "Keamanan SSL/HTTPS",
        "Jasa Pembuatan Website",
        "Program Afiliasi",
      ],
    },
  ]);

  const [list2] = useState([
    {
      title: "Service Hosting",
      item: [
        "Hosting Murah",
        "Hosting Indonesia",
        "Hosting Singapore SG",
        "Hosting PHP",
        "Hosting Wordpress",
        "Hosting Laravel",
      ],
    },
  ]);

  const [list3] = useState([
    {
      title: "Tutorial",
      item: ["Knowledgebase", "Blog", "Cara Pembayaran"],
    },
  ]);

  const [list4] = useState([
    {
      title: "Tentang Kami",
      item: [
        "Tim Niagahoster",
        "Karir",
        "Events",
        "Penawaran & Promo Spesial",
        "Kontak Kami",
      ],
    },
  ]);

  const [list5] = useState([
    {
      title: "Kenapa Pilih Niagahoster",
      item: [
        "Support Terbaik",
        "Garansi Harga Termurah",
        "Domain Gratis Selamanya",
        "Datacenter Hosting Terbaik",
        "Review Pelanggan",
      ],
    },
  ]);

  return (
    <footer id="footer" className="nh-footer">
      <div className="container">
        <div className="footer-wrap">
          <div className="top">
            <h5 className="footer-title">Hubungi Kami</h5>
            <p className="footer-contact">
              Telp: 0274-5305505
              <br />
              WA: 0895422447394
              <br />
              Senin - Minggu
              <br />
              24 Jam Non Stop
            </p>
            <p className="footer-address">
              Jl. Selokan Mataram Monjali
              <br />
              Karangjati MT I/304
              <br />
              Sinduadi, Mlati, Sleman
              <br />
              Yogyakarta 55284
            </p>
          </div>
          <FooterTop list={list1} />
          <FooterTop list={list2} />
          <FooterTop list={list3} />
        </div>
        <div className="footer-wrap center-wrap">
          <FooterTop list={list4} />
          <FooterTop list={list5} />
          <div className="top">
            <h5 className="footer-title">Newsletter</h5>
            <form action="" className="footer-newsletter">
              <input
                className="newsletter-input"
                name="email"
                type="text"
                placeholder="your email@gmail.com"
              />
            </form>
            <div className="newsletter-btn">
              <button className="news-btn">BERLANGGANAN</button>
            </div>
          </div>
          <div className="top">
            <ul className="social-media">
              <li>
                <a
                  className="icon-fb"
                  href="https://www.facebook.com/niagahoster"
                  target="blank"
                >
                  <i className="fab" alt="facebook">
                    <FontAwesomeIcon icon={faFacebookF} />
                  </i>
                </a>
              </li>
              <li>
                <a
                  className="icon-ig"
                  href="https://www.instagram.com/niagahoster.id"
                  target="blank"
                >
                  <i className="fab" alt="instagram">
                    <FontAwesomeIcon icon={faInstagram} />
                  </i>
                </a>
              </li>
              <li>
                <a
                  className="icon-li"
                  href="https://www.linkedin.com/company/niagahoster"
                  target="blank"
                >
                  <i className="fab" alt="linkedin">
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </i>
                </a>
              </li>
              <li>
                <a
                  className="icon-twitter"
                  href="https://twitter.com/niagahoster"
                  target="blank"
                >
                  <i className="fab" alt="twitter">
                    <FontAwesomeIcon icon={faTwitter} />
                  </i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="footer-wrap bottom-wrap">
          <div className="bottom-left">
            <p>
              Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux,
              CloudFlare, BitNinja and DC DCI-Indonesia. Cloud VPS Murah powered
              by Webuzo Softaculous, Intel SSD and cloud computing technology
            </p>
          </div>
          <div className="bottom-right">
            <div className="privacy-policy">
              <a href="#footer">Syarat dan Ketentuan </a>
              <span> | </span>
              <a href="#footer"> Kebijakan Privasi</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
